<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php 
	$form = ActiveForm::begin(); 
	print $form->field($model, 'name');
	print $form->field($model,'email');
?>
<div class="form-group">
	<?php print Html::submitButton('submit',['class'=> 'btn btn-primary']) ?>
</div>
<?php
	ActiveForm::end()
?>
