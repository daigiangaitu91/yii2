<?php

use yii\helpers\Html;
?>

<ul>
	<li>
		<label>Name</label> : <?php print Html::encode($model->name) ?>
		<label>Email</label> : <?php print Html::encode($model->email) ?>
	</li>
</ul>